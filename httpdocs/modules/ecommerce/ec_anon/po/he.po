# $Id: he.po,v 1.1.2.2 2007/07/08 00:55:18 gordon Exp $
#
# Hebrew translation of Drupal (ec_anon.module)
# Copyright 2007 Amnon Levav http://www.levavie.com
# Generated from files:
#  ec_anon.module,v 1.1.2.4.2.15 2007/03/06 11:25:06 sammys
#  ec_anon.install,v 1.1.2.4.2.5 2007/02/16 01:40:57 sime
#  ec_anon.info,v 1.1.2.11 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ecommerce\n"
"POT-Creation-Date: 2007-05-03 10:42-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: Amnon <www.levavie.com>\n"
"Language-Team: Hebrew <www.levavie.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ec_anon.module:61
msgid "LoginToboggan module"
msgstr "רכיב LoginToboggan"

#: ec_anon.module:61
msgid "LoginToboggan settings page"
msgstr "עמוד הגדרות LoginToboggan"

#: ec_anon.module:80
msgid "email confirmation"
msgstr "אישור כתובת דואר אלקטרוני"

#: ec_anon.module:87
msgid "confirmation sent"
msgstr "הודעת האישור נשלחה"

#: ec_anon.module:104
msgid "Anonymous purchases"
msgstr "קניות ללא הרשמה"

#: ec_anon.module:113
msgid "Anonymous purchasing policy for this product"
msgstr "מדיניות קנייה ללקוחות שאינם רשומים באתר עבור מוצר זה"

#: ec_anon.module:116
msgid "This sets the policy for user registrations during checkout for this product. This setting overrides the !link, which is currently set to <i>!setting</i>.<br/>"
msgstr "זה מגדיר מדיניות לגבי הרשמת לקוח לאתר תוך כדי סיום הזמנת המוצר. הגדרה זו מבטלת את ההגדרה !link, שנמצאת כרגע במצב <i>!setting</i>."

#: ec_anon.module:116
msgid "site-wide setting"
msgstr "הגדרות לכל האתר"

#: ec_anon.module:121
msgid "Anonymous purchasing policy"
msgstr "הגדרות קנייה ללקוחות שאינם רשומים באתר"

#: ec_anon.module:124
msgid "This sets the site-wide policy for anonymous purchasing. This setting can be overridden by product settings.<br/>"
msgstr "זה קובע את הגדרות הקנייה ללקוחות שאינם רשומים. אפשר לשנות הגדרות אלו ברמת המוצר.<br/>"

#: ec_anon.module:156
msgid "Please follow the link contained in that email then proceed to the checkout to complete your purchase."
msgstr "בבקשה לחץ על הקישור בהודעה זו כדי להמשיך את תהליך הקנייה ולהשלים את ההזמנה."

#: ec_anon.module:158
msgid "Please continue with the checkout process."
msgstr "בבקשה להמשיך את ההזמנה."

#: ec_anon.module:169
msgid "Registered only"
msgstr "רשומים בלבד."

#: ec_anon.module:170
msgid "Flexible"
msgstr "גמישה"

#: ec_anon.module:171
msgid "Anonymous only"
msgstr "לקוחות שאינם רשומים בלבד"

#: ec_anon.module:175
msgid "Unset"
msgstr "לא מוגדרת"

#: ec_anon.module:183
msgid "\n<ul>\n  <li><i>%never:</i> Anonymous purchases are disabled. All customers must register or login before they can checkout.</li>\n  <li><i>%optional:</i> Customers can choose to register, login or checkout anonymously.</li>\n  <li><i>%always:</i> Only anonymous purchases are allowed. Customers are never given the option to register during checkout. Customers will be given the option of logging in or purchasing anonymously.</li>\n</ul>"
msgstr ""

#: ec_anon.module:253
msgid "Anonymous policy for this product has been reset to %policy."
msgstr "עודכנה מדיניות רכישת המוצע עבור לקוחות שאינם רשומים. המדיניות החדשה: %policy"

#: ec_anon.module:305
msgid "Only non-registered users can purchase this product"
msgstr "רק לקוחות שאינם רשומים יכולים לרכוש מוצר זה"

#: ec_anon.module:309
msgid "Only registered users can purchase this product"
msgstr "רק לקוחות רשומים יכולים לרכוש מוצר זה"

#: ec_anon.module:424
msgid "Email"
msgstr "דואר אלקטרוני"

#: ec_anon.module:427
msgid "Continue"
msgstr "המשך"

#: ec_anon.module:430
msgid "<b>OR</b> if you already have an account"
msgstr "<b>או</b> אם אתה כבר רשומ/ה באתר"

#: ec_anon.module:433
msgid "Username"
msgstr "כינוי"

#: ec_anon.module:437
msgid "Password"
msgstr "סיסמה"

#: ec_anon.module:440;462
msgid "Login"
msgstr "כניסה"

#: ec_anon.module:448
msgid "Register for an account"
msgstr "הרשמה כלקוח"

#: ec_anon.module:466
msgid "Please supply a username"
msgstr "בבקשה לרשום את הכינוי שלך באתר"

#: ec_anon.module:469
msgid "Please supply a password"
msgstr "בבקשה לרשום סיסמה"

#: ec_anon.module:502
msgid "Sorry. Your account is currently blocked. Please contact the store by phone or email to arrange your purchase"
msgstr "מצטערים. החשבון שלך חסום כעת. בבקשה צור עם החנות קשר בטלפון או בדואר האלקטרוני כדי לארגן את הזמנתך."

#: ec_anon.module:515
msgid "Sorry. Unrecognized username or password."
msgstr "מצטערים. כינוי המשתמש או הסיסמה אינם מוכרים."

#: ec_anon.module:515
msgid "Have you forgotten your password?"
msgstr "האם שכחת את סיסמתך?"

#: ec_anon.module:531
msgid "The e-mail address %email is already taken."
msgstr "יש כבר חבר באתר עם הדואר האלקטרוני %email "

#: ec_anon.module:543
msgid "(change)"
msgstr "(שינוי)"

#: ec_anon.module:546
msgid "Email address"
msgstr "כתובת דואר אלקטרוני"

#: ec_anon.module:0
msgid "ec_anon"
msgstr ""

#: ec_anon.install:29
msgid "E-Commerce: Anon tables have been created."
msgstr "סחר אלקטרוני: נוצאו טבלאות עבור רכיב ec_anon"

#: ec_anon.install:41
msgid "ec_anon module has been set as the first checkout screen"
msgstr "רכיב ec_anon נקבע כמסך ההזמנה הראשון."

#: ec_anon.install:55
msgid "Anonymous purchasing policy for the site has been set to <b>!policy</b>. You can change it on the <a href=\"@settingsurl\">store settings page</a>."
msgstr "מדיניות הרכישה ללקוחות שאינם רשומים כחברים באתר נקבעה ל<b>!policy</b>. ניתן לשנות מדיניות זו ב<a href=\"@settingsurl\">עמוד הגדרות החנות</a>."

#: ec_anon.info:0
msgid "Anonymous purchasing"
msgstr "קנייה ללקוחות לא רשומים"

#: ec_anon.info:0
msgid "Implements anonymous purchasing policy settings and enforcement."
msgstr "מיישם מדיניות רכישה ואכיפה עבור לקוחות שאינם רשומים."

#: ec_anon.info:0
msgid "E-Commerce Core"
msgstr "ליבת הסחר האלקטרוני"

