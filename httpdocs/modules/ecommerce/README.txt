********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Ecommerce Package

Current Maintainer : Gordon Heydon http://drupal.org/user/959
Original Author    : Matt Westgate (not a current maintainer)

General Links:
Project Page       : http://drupal.org/project/ecommerce
Handbook           : http://drupal.org/node/50350
Support Queue      : http://drupal.org/project/issues/ecommerce
IRC                : irc.freenode.org > #drupal-ecommerce
Mailing List       : http://lists.heydon.com.au/listinfo.cgi/ecommerce-heydon.com.au
                     (Mailing list subject to change)

Developer Links:
Developers API     : http://ecommerce.heydon.com.au/api/ecommerce/function
Developers Docs    : http://drupal.org/node/52325
                     (Handbook pages are in development)

********************************************************************
DESCRIPTION:

This package aims to provide a framework for a complete ecommerce
solution for your website.

Product modules are available that allow you to sell a variety of product
types, from physical "tangible" products to virtual "files" or
perhaps "parcels" containing multiple items.

Sammys Spets (Synerger) has contributed a new email subsystem, a recurring
payments and anonymous purchasing modules.

Gordon Heydon (Heydon Consulting) has contributed a new invoicing system
with PDF support and more.

James Gilliland has done a lot of refactoring of the store.module and the
cart module.

Please check the release notes for full details of each release.

A transaction system allows you to track and manage your orders,
changing the status as necessary and sending invoices and shipping
notifications.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running. If you are
having problems, please review the Handbook, especially
http://drupal.org/node/43767. Besides Drupal core and ecommerce itself,
token.module (http://drupal.org/project/token) is also required to be installed 
and active.

Preparing for Installation:
---------------------------
Note: Please back up your site and database.

1. Download and install the token module:
   http://drupal.org/project/token

2. Download and place the entire ecommerce directory into your desired contrib
   modules location: eg. /sites/all/modules/

3. Enable core modules by going to:
   > Administer > Site Building > Modules.

   With E-Commerce 5, there are the modules are grouped according to function.
   The first module to install is Store module. This will install every other
   mandatory E-Commerce module.

4. Having installed the main modules, now install the other modules you'd like activated.
   You'll at least want to install a product module like Generic, and a payment
   module like COD.

5. Drupal 5 introduced a cleaner administration section. You can find E-Commerce
   settings in two blocks - Just click on Administer then click on:

   E-Commerce -- day to administration of your store.
   E-Commerce configuration -- settings for setting up your store.

   Individual module settings are also listed there with their descriptions.

6. Grant the proper access to user accounts under:
   > Administer > User Management > Access control

7. Create new products via:
   > Create content > Product

8. Optionally, enable the cart block via
   > Administer > blocks :: Shopping Cart

