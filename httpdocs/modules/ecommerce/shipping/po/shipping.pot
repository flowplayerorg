# $Id: shipping.pot,v 1.1.2.1 2007/05/03 14:52:05 darrenoh Exp $
#
# LANGUAGE translation of Drupal (shipping.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  shipping.module,v 1.43.2.3.2.1.2.11 2007/04/25 04:38:05 gordon
#  shipping.install,v 1.5.4.5.2.3 2007/02/15 05:38:48 sammys
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:42-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: shipping.module:15;104
msgid "Shipping email"
msgstr ""

#: shipping.module:15
msgid "This text will be emailed to customer after their order has been shipped."
msgstr ""

#: shipping.module:32;499
msgid "Fulfillment centers"
msgstr ""

#: shipping.module:33
msgid "These are the locations from which you ship. Currently used by Shipping API calculations like UPS."
msgstr ""

#: shipping.module:39
msgid "List"
msgstr ""

#: shipping.module:47
msgid "Add"
msgstr ""

#: shipping.module:59;346
msgid "Shipping"
msgstr ""

#: shipping.module:60
msgid "Shipping notification page."
msgstr ""

#: shipping.module:62
msgid "Shipping notification"
msgstr ""

#: shipping.module:72;79;465
msgid "Edit"
msgstr ""

#: shipping.module:97
msgid "Subject of shipping e-mail"
msgstr ""

#: shipping.module:98;698
msgid "Your %site order has shipped (#%txnid)"
msgstr ""

#: shipping.module:108
msgid "This text will be emailed to customers after their order has been shipped."
msgstr ""

#: shipping.module:126
msgid "Shipping methods"
msgstr ""

#: shipping.module:127
msgid "Select the shipping methods you wish to make available for this product."
msgstr ""

#: shipping.module:323
msgid "Please select a shipping method."
msgstr ""

#: shipping.module:380
msgid "Choose shipping method"
msgstr ""

#: shipping.module:400
msgid "%price %currency - %method"
msgstr ""

#: shipping.module:410
msgid "Continue"
msgstr ""

#: shipping.module:445;541
msgid "Name"
msgstr ""

#: shipping.module:446
msgid "Code"
msgstr ""

#: shipping.module:447;550
msgid "City"
msgstr ""

#: shipping.module:448
msgid "Region"
msgstr ""

#: shipping.module:449;576
msgid "Country"
msgstr ""

#: shipping.module:450;464;534
msgid "Default"
msgstr ""

#: shipping.module:451
msgid "Operations"
msgstr ""

#: shipping.module:465;639
msgid "Delete"
msgstr ""

#: shipping.module:490
msgid "There are no fulfillment centers configured."
msgstr ""

#: shipping.module:522;589
msgid "Edit fulfillment center"
msgstr ""

#: shipping.module:522;589
msgid "Add fulfillment center"
msgstr ""

#: shipping.module:536
msgid "Check this box if this is the default fulfillment center.  (<em>Currently all shipments are sent from the default fulfillment center.</em>)"
msgstr ""

#: shipping.module:546
msgid "The name of the fulfillment center."
msgstr ""

#: shipping.module:555
msgid "The city that the fulfillment center is located in."
msgstr ""

#: shipping.module:559
msgid "Region (State/Province)"
msgstr ""

#: shipping.module:564
msgid "The region (state/province) that the fulfillment center is located in."
msgstr ""

#: shipping.module:568
msgid "Zip/Postal code"
msgstr ""

#: shipping.module:572
msgid "The fulfillment center zipcode or postal code."
msgstr ""

#: shipping.module:580
msgid "The country that the fulfillment center is located in."
msgstr ""

#: shipping.module:593;640
msgid "Cancel"
msgstr ""

#: shipping.module:618
msgid "Fulfillment center updated."
msgstr ""

#: shipping.module:622
msgid "Fulfillment center added."
msgstr ""

#: shipping.module:636
msgid "Confirm deletion of fulfillment center <em>%shipfrom</em>"
msgstr ""

#: shipping.module:638
msgid "Click the <em>Delete</em> button to permanently delete the <em>%shipfrom</em> fulfillment center. This action cannot be undone."
msgstr ""

#: shipping.module:671
msgid "No shipping default fulfillment center configured, shipping prices will not be accurate."
msgstr ""

#: shipping.module:688
msgid "Shipment notification"
msgstr ""

#: shipping.module:699
msgid "Hello %first_name,\n\nWe have shipped the following item(s) from Order #%txnid, received  %order_date.\n\nItems(s) Shipped:\n%items\n%shipping_to\nQuestions about your order? Please contact us at %email.\n\nThanks for shopping at %site.  We hope to hear from you again real soon!\n\n%uri"
msgstr ""

#: shipping.install:103
msgid "E-Commerce: Shipping tables have been created."
msgstr ""

#: shipping.info:0
msgid "Shipping API"
msgstr ""

#: shipping.info:0
msgid "Enable a shipping module API for ecommerce. You must also install and enable at least one shipping module to use the API."
msgstr ""

#: shipping.info:0
msgid "E-Commerce Core"
msgstr ""

