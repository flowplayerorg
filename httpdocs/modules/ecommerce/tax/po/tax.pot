# $Id: tax.pot,v 1.1.2.1 2007/05/03 14:52:06 darrenoh Exp $
#
# LANGUAGE translation of Drupal (tax.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  tax.module,v 1.16.2.4.4.10 2007/03/11 12:32:06 gordon
#  tax.install,v 1.3.4.1.2.1 2007/02/14 06:48:25 sime
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:45-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: tax.module:14
msgid "Enter tax rules here."
msgstr ""

#: tax.module:16
msgid "tax calculation methods are <a href=\"!tax_calc_url\">here</a>."
msgstr ""

#: tax.module:31;37
msgid "Taxes"
msgstr ""

#: tax.module:33
msgid "Configure tax rules"
msgstr ""

#: tax.module:44
msgid "Add tax"
msgstr ""

#: tax.module:52
msgid "Edit tax"
msgstr ""

#: tax.module:60
msgid "Delete tax"
msgstr ""

#: tax.module:68
msgid "Tax autocomplete"
msgstr ""

#: tax.module:85
msgid "Tax"
msgstr ""

#: tax.module:149
msgid "rule"
msgstr ""

#: tax.module:149
msgid "operations"
msgstr ""

#: tax.module:153
msgid "Add %operator%operand where %realm = '%realm-value'"
msgstr ""

#: tax.module:153
msgid "edit"
msgstr ""

#: tax.module:153
msgid "delete"
msgstr ""

#: tax.module:157
msgid "There are currently no tax rules."
msgstr ""

#: tax.module:193;316
msgid "Next"
msgstr ""

#: tax.module:199
msgid "You must enter a location value."
msgstr ""

#: tax.module:203
msgid "You must choose a product type."
msgstr ""

#: tax.module:219
msgid "Invalid operator"
msgstr ""

#: tax.module:223
msgid "You must enter a rate."
msgstr ""

#: tax.module:229;404
msgid "Submit"
msgstr ""

#: tax.module:229;397
msgid "Update"
msgstr ""

#: tax.module:243
msgid "Are you sure you want to delete this tax rule"
msgstr ""

#: tax.module:258
msgid "tax rule deleted"
msgstr ""

#: tax.module:303;312
msgid "State / Province / Region"
msgstr ""

#: tax.module:304;339;342
msgid "Country"
msgstr ""

#: tax.module:305
msgid "City"
msgstr ""

#: tax.module:309
msgid "Type of Location to define the new tax rule for"
msgstr ""

#: tax.module:322;350
msgid "This value will be checked against the customer's shipping address."
msgstr ""

#: tax.module:326
msgid "Value for State / Province / Region"
msgstr ""

#: tax.module:336
msgid "Please choose..."
msgstr ""

#: tax.module:368
msgid "Adjustment"
msgstr ""

#: tax.module:371
msgid "This rate can be a simple price addition or a percentage multiplier. For example, to add a 5.00 tax, enter +5.00. To multiply the gross price times 75%, enter 75%. If no operand is given, addition is assumed."
msgstr ""

#: tax.module:385
msgid "Product type"
msgstr ""

#: tax.module:388
msgid "Check the product types this tax rule applies to."
msgstr ""

#: tax.module:0
msgid "tax"
msgstr ""

#: tax.install:33
msgid "E-Commerce: Tax tables have been created."
msgstr ""

#: tax.info:0
msgid "Tax API"
msgstr ""

#: tax.info:0
msgid "Enable a simple tax API for ecommerce."
msgstr ""

#: tax.info:0
msgid "E-Commerce Uncategorized"
msgstr ""

