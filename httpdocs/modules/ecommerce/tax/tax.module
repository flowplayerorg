<?php
// $Id: tax.module,v 1.16.2.4.4.10 2007/03/11 12:32:06 gordon Exp $

/********************************************************************
 * Drupal Hooks
 ********************************************************************/

/**
 * Implementation of hook_help()
 */
function tax_help($section = 'admin/help#tax') {
  switch ($section) {
    case 'admin/ecsettings/tax':
      return t('Enter tax rules here.');
    case 'admin/settings/tax':
      return t('tax calculation methods are <a href="!tax_calc_url">here</a>.', array('!tax_calc_url' => url('admin/ecsettings/tax')));
  }

  return $output;
}

/**
 * Implementation of hook_menu()
 */
function tax_menu($may_cache) {
  $items = array();
  $access = user_access('administer store');
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/ecsettings/tax', 
      'title' => t('Taxes'), 'access' => $access, 
      'callback' => 'tax_admin_overview',
      'description' => t('Configure tax rules'),
    );
    $items[] = array(
      'path' => 'admin/ecsettings/tax/list', 
      'title' => t('Taxes'), 
      'access' => $access, 
      'type' => MENU_DEFAULT_LOCAL_TASK, 
      'weight' => -10
    );
    $items[] = array(
      'path' => 'admin/ecsettings/tax/add', 
      'title' => t('Add tax'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('tax_admin_form', 0),
      'access' => $access, 
      'type' => MENU_LOCAL_TASK
    );
    $items[] = array(
      'path' => 'admin/ecsettings/tax/edit', 
      'title' => t('Edit tax'), 
      'access' => $access, 
      'callback' => 'drupal_get_form',
      'callback arguments' => array('tax_admin_form'),
      'type' => MENU_CALLBACK
    );
    $items[] = array(
      'path' => 'admin/ecsettings/tax/delete', 
      'title' => t('Delete tax'), 
      'access' => $access, 
      'callback' => 'drupal_get_form',
      'callback arguments' => array('tax_admin_delete'),
      'type' => MENU_CALLBACK
    );
    $items[] = array(
      'path' => 'admin/ecsettings/tax/add/autocomplete', 
      'title' => t('Tax autocomplete'), 
      'callback' => 'tax_autocomplete_state', 
      'access' => $access, 
      'type' => MENU_CALLBACK
    );
  }
  return $items;
}

/**
 * Implementation of hook_checkoutapi().
 */
function tax_checkoutapi(&$txn, $op, $arg3 = NULL, $arg4 = NULL) {
  $output = '';
  switch ($op) {
    case 'review':
      if ($txn == 'tax') return TRUE;
      $box['subject'] = t('Tax');
      $billing = $txn->address['billing'];
      $rules = tax_get_rules();
      $states = store_build_states(variable_get('ec_country', 'us'));
      $total_tax = 0;
      $taxable_amount = 0;

      foreach ($txn->items as $item) {
        if (product_is_shippable($item->vid)) {
          $taxable_amount += store_adjust_misc($txn, $item) * $item->qty;
        }
      }

      foreach ($rules as $rule) {
        switch ($rule->realm) {
          case 'city':
            if (drupal_strtoupper($billing->city) == drupal_strtoupper($rule->realm_value) && tax_rule_product_match($txn, $rule)) {
              if ($tax = tax_calculate($taxable_amount, $rule->operand, $rule->operator)) {
                $total_tax += $tax;
              }
            }
            break;
          case 'country':
            if (drupal_strtoupper($billing->country) == drupal_strtoupper($rule->realm_value) && tax_rule_product_match($txn, $rule)) {
              if ($tax = tax_calculate($taxable_amount, $rule->operand, $rule->operator)) {
                $total_tax += $tax;
              }
            }
            break;
          case 'state':
            if ($billing->state) {
              if (drupal_strtoupper($states[$billing->state]) == drupal_strtoupper($rule->realm_value) && tax_rule_product_match($txn, $rule)) {
                if ($tax = tax_calculate($taxable_amount, $rule->operand, $rule->operator)) {
                  $total_tax += $tax;
                }
              }
            }
            break;
        }
      }
      if ($total_tax) {
        if (($key = store_search_misc(array('type' => 'tax'), $txn)) !== false) {
          $txn->misc[$key]->price = round($total_tax, 2);
        }
        else {
          $misc = array(
            'type' => 'tax',
            'description' => 'Tax',
            'price' => round($total_tax, 2),
            'weight' => 10
          );
          $txn->misc[] = (object)$misc;
        }
      }

      return;
  }
}

/**
 * Show an overview of all the tax rules.
 */
function tax_admin_overview() {
  $output = '';
  $header = array(t('rule'), t('operations'));

  $result = db_query('SELECT * FROM {ec_tax}');
  while ($rule = db_fetch_object($result)) {
    $rows[] = array(t("Add %operator%operand where %realm = '%realm-value'", array('%operator' => $rule->operator, '%operand' => ($rule->operand == '+' ? '' : '%'), '%realm' => $rule->realm, '%realm-value' => $rule->realm_value)), l(t('edit'), "admin/ecsettings/tax/edit/$rule->taxid"). ' | '. l(t('delete'), "admin/ecsettings/tax/delete/$rule->taxid"));
  }

  if (count($rows) == 0) {
    $rows[] = array(array('data' => '<em>'. t('There are currently no tax rules.') .'</em>', 'colspan' => 5));
  }
  $output .= theme('table', $header, $rows);

  return $output;
}

function tax_admin_form($taxid = NULL, $form_values = NULL) {
  if ($taxid) {
    if ($taxitem = tax_get_rule($taxid)) {
      $form = array_merge(tax_form_screen1($taxitem), tax_form_screen2($taxitem));
    }
    else {
      drupal_not_found();
      exit();
    }
  }
  else {
    if (!$form_values) {
      $form = tax_form_screen1($form_values);
    }
    else {
      $form = tax_form_screen2($form_values);
    }

    $form['#multistep'] = TRUE;
  }

  return $form;
}

/**
 * Validate a tax rule.
 */
function tax_admin_form_validate($form_id, $form_values, $form) {
  switch ($form_values['op']) {
    case t('Next'):
      break;

    default:
      $errors = array();
      if (!$form_values['realm_value']) {
        form_set_error('realm_value', t('You must enter a location value.'));
      }

      if (empty($form_values['ptype'])) {
        form_set_error('ptype', t('You must choose a product type.'));
      }

      if ($form_values['rate']) {
        if (strstr($form_values['rate'], '%')) {
          $form_values['operand'] = '%';
          $form_values['operator'] = rtrim($form_values['rate'], '%');
        }
        else {
          $form_values['operand'] = '+';
          $form_values['operator'] = ltrim($form_values['rate'], '+');
        }
        form_set_value($form['operand'], $form_values['operand']);
        form_set_value($form['operator'], $form_values['operator']);

        if (!is_numeric($form_values['operator'])) {
          form_set_error('rate', t('Invalid operator'));
        }
      }
      else {
        form_set_error('rate', t('You must enter a rate.'));
      }
  }
}

function tax_admin_form_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Submit') || $form_values['op'] == t('Update')) {
    tax_save_rule($form_values);
    return 'admin/ecsettings/tax';
  }
  return FALSE;
}

function tax_admin_delete($taxid) {
  if ($taxid) {
    if ($taxitem = tax_get_rule($txnid)) {
      $form['taxid'] = array(
        '#type' => 'value',
        '#value' => $taxid,
      );
      return confirm_form($form, t('Are you sure you want to delete this tax rule'), 'admin/ecsettings/tax');
    }
    else {
      drupal_not_found();
      exit();
    }
  }
  else {
    drupal_access_denied();
    exit();
  }
}

function tax_admin_delete_submit($form_id, $form_values) {
  db_query('DELETE FROM {ec_tax} WHERE taxid = %d', $form_values['taxid']);
  drupal_set_message(t('tax rule deleted'));
  return 'admin/ecsettings/tax';
}

/********************************************************************
 * Module Functions :: Save Routines
 ********************************************************************/

function tax_save_rule($edit) {
  $fields = tax_fields();
  if ($edit['taxid'] > 0 && db_result(db_query('SELECT COUNT(taxid) FROM {ec_tax} WHERE taxid = %d', $edit['taxid']))) {
    // Prepare the query:
    foreach ($edit as $key => $value) {
      if (in_array($key, $fields)) {
        if ($key == 'ptype') {
          $value = implode(',', $value);
        }
        $q[] = db_escape_string($key) ." = '%s'";
        $v[] = $value;
      }
    }

    db_query("UPDATE {ec_tax} SET ". implode(', ', $q) ." WHERE taxid = ". (int) $edit['taxid'], $v);
  }
  else {
    $edit['taxid'] = db_next_id('{ec_tax}_taxid');
    // Prepare the query:
    foreach ($edit as $key => $value) {
      if (in_array((string) $key, $fields)) {
        if ($key == 'ptype') {
          $value = implode(',', $value);
        }
        $k[] = db_escape_string($key);
        $v[] = $value;
        $s[] = "'%s'";
      }
    }
    
    db_query("INSERT INTO {ec_tax} (". implode(", ", $k) .") VALUES(". implode(", ", $s) .")", $v);
  }
}

function tax_form_screen1($edit = array()) {
  $name = tax_invoke_taxapi($edit, 'name');
  $locations = array(
    'state' => t('State / Province / Region'), 
    'country' => t('Country'), 
    'city' => t('City')
  );
  $form['realm'] = array(
    '#type' => 'select',
    '#title' => t('Type of Location to define the new tax rule for'),
    '#default_value' => $edit['realm'],
    '#options' => $locations,
    '#description' => t('State / Province / Region'),
  );
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Next')
  );
  return $form;
}

function tax_form_screen2($edit = array()) {
  $help = t('This value will be checked against the customer\'s shipping address.');
  if ($edit['realm'] == 'state') {
    $form['realm_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value for State / Province / Region'),
      '#default_value' => $edit['realm_value'],
      '#size' => 32,
      '#maxlength' => 32,
      '#autocomplete_path' => 'admin/ecsettings/tax/add/autocomplete',
      '#description' => $help,
    );
  }
  elseif ($edit['realm'] == 'country') {
    $country = store_build_countries();
    array_unshift($country, t('Please choose...'));
    $form['realm_value'] = array(
      '#type' => 'select',
      '#title' => t('Country'),
      '#default_value' => ($edit['realm_value'] ? $edit['realm_value'] : variable_get('ec_country', '')),
      '#options' => $country,
      '#description' => t('Country'),
    );
  }
  elseif ($edit['realm'] == 'city') {
    $form['realm_value'] = array(
      '#type' => 'textfield',
      '#default_value' => $edit['realm_value'], 
      '#maxlength' =>  32, 
      '#description' => t('This value will be checked against the customer\'s shipping address.'), 
    );
  }
  else {
    #$form = tax_invoke_taxapi($edit, 'admin form');
    #$output .= $form[0];
    
    /*
    * The above is not solved yet, don't really know what this call is for?!
    * Should there ever be an else to this structure?
    */
  }
  $form['realm'] = array(
    '#type' => 'hidden', 
    '#value' => $edit['realm']
  );
  $form['rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Adjustment'),
      '#default_value' => $edit['rate'], 
      '#maxlength' =>  13, 
      '#description' => t('This rate can be a simple price addition or a percentage multiplier. For example, to add a 5.00 tax, enter +5.00. To multiply the gross price times 75%, enter 75%. If no operand is given, addition is assumed.'), 
    );
  $form['operator'] = array(
    '#type' => 'value',
    '#value' => $edit['operator'],
  );
  $form['operand'] = array(
    '#type' => 'value',
    '#value' => $edit['operand'],
  );
  $ptypes = product_get_ptypes();
  asort($ptypes);
   $form['ptype'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Product type'),
    '#default_value' => $edit['ptype'],
    '#options' => $ptypes,
    '#description' => t('Check the product types this tax rule applies to.'),
  );
  if ($edit['taxid']) {
    $form['taxid'] = array(
      '#type' => 'value', 
      '#value' => $edit['taxid']
    );
    $form['submit'] = array(
      '#type' => 'submit', 
      '#value' => t('Update'),
      '#weight' => 10,
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit', 
      '#value' => t('Submit'),
      '#weight' => 10,
    );
  }
  return $form;
}

/**
 * Retrieve a pipe delimited string of autocomplete suggestions for existing locations
 */
function tax_autocomplete_state($string) {
  $states = store_build_states();
  $matches = array();
  foreach ($states as $country) {
    if ($state = preg_grep("/^$string(.+?)/i", $country)) {
      $matches = array_merge($matches, $state);
    }
  }

  print drupal_to_js($matches);
  exit();
}

/**
 * Invoke a hook_nodeapi() operation in all modules.
 *
 * @param &$txn
 *   A txn object.
 * @param $op
 *   A string containing the name of the taxapi operation.
 * @param $a3, $a4
 *   Arguments to pass on to the hook, after the $txn and $op arguments.
 * @return
 *   The returned value of the invoked hooks.
 */
function tax_invoke_taxapi(&$txn, $op, $a3 = NULL, $a4 = NULL) {
  $return = array();
  foreach (module_list() as $name) {
    $function = $name .'_taxapi';
    if (function_exists($function)) {
      $result = $function($txn, $op, $a3, $a4);
      if (is_array($result)) {
        $return = array_merge($return, $result);
      }
      else if (isset($result)) {
        $return[] = $result;
      }
    }
  }
  return $return;
}

/**
 * Get tax rule from the DB.
 */
function tax_get_rule($tid) {
  $result = db_query('SELECT * FROM {ec_tax} WHERE taxid = %d', (int) $tid);
  $rule = db_fetch_array($result);
  $rule['rate'] = $rule['operand'] == '%' ? $rule['operator'] . $rule['operand'] : $rule['operand'] . $rule['operator'];
  $rule['ptype'] = explode(',', $rule['ptype']);
  return $rule;
}

/**
 * Get all rules from the DB.
 */
function tax_get_rules() {
  $rules = array();
  $result = db_query('SELECT * FROM {ec_tax}');
  while ($rule = db_fetch_object($result)) {
    $rules[] = $rule;
  }
  return $rules;
}

/**
 * Return the tax cost for a given rule.
 */
function tax_calculate($gross, $operand, $op2) {
  switch ($operand) {
    case '+':
      return $op2;
    case '%':
      return($gross * ($op2/100));
  }
}

/**
 * Return TRUE if $rule applies to the current products in the cart.
 */
function tax_rule_product_match($txn, $rule) {
  $ptypes = explode(',', $rule->ptype);
  foreach ($txn->items as $item) {
    if (in_array($item->ptype, $ptypes)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Returnt the table names in the database.
 */
function tax_fields() {
  return array('taxid', 'realm', 'realm_value', 'ptype', 'operator', 'operand');
}
