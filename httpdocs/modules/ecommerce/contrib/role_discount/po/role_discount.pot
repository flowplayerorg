# $Id: role_discount.pot,v 1.1.2.1 2007/05/03 15:12:24 darrenoh Exp $
#
# LANGUAGE translation of Drupal (role_discount.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  role_discount.module,v 1.2.2.3.4.6 2007/02/20 12:42:17 sime
#  role_discount.install,v 1.2.6.1 2007/02/14 06:48:25 sime
#  role_discount.info,v 1.3.2.8 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 11:07-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: role_discount.module:23
msgid "Role discount"
msgstr ""

#: role_discount.module:24
msgid "Configure store discounts by role."
msgstr ""

#: role_discount.module:28
msgid "Configure discounts based upon user role"
msgstr ""

#: role_discount.module:41
msgid "<p>You may define a price adjustment for each role. This adjustment can be a simple price addition, subtraction, or a percentage multiplier. For example, to add 5.00 to every price, enter +5.00. To multiply every price times 75%, enter 75%. If no operator is given, addition is assumed.</p>"
msgstr ""

#: role_discount.module:84
msgid "Save price adjustments"
msgstr ""

#: role_discount.module:93
msgid "Role"
msgstr ""

#: role_discount.module:94
msgid "Amount"
msgstr ""

#: role_discount.module:150
msgid "Price adjustments have been updated."
msgstr ""

#: role_discount.module:0
msgid "role_discount"
msgstr ""

#: role_discount.install:32
msgid "E-Commerce: Role Discount tables have been created."
msgstr ""

#: role_discount.info:0
msgid "Role Discount"
msgstr ""

#: role_discount.info:0
msgid "Role discounts for E-Commerce."
msgstr ""

#: role_discount.info:0
msgid "E-Commerce Uncategorized"
msgstr ""

