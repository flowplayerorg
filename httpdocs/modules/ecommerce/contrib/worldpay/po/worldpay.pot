# $Id: worldpay.pot,v 1.1.2.1 2007/05/03 15:12:25 darrenoh Exp $
#
# LANGUAGE translation of Drupal (worldpay.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  worldpay.module,v 1.9.2.1.2.1.2.2 2007/02/03 01:38:19 gordon
#  worldpay.info,v 1.1.2.7 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 11:09-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: worldpay.module:19
msgid "The worldpay module allows the Drupal ecommerce system to use WorldPay as a payment gateway.  In order to use Worldpay as a payment gateway, you will first need to have the ecommerce module installed and configured, and a WorldPay account set up."
msgstr ""

#: worldpay.module:33
msgid "Main settings"
msgstr ""

#: worldpay.module:43
msgid "Installation ID (instId)"
msgstr ""

#: worldpay.module:47
msgid "WorldPay Installation ID. This should be set to your unique ID, which was given to you by WorldPay."
msgstr ""

#: worldpay.module:54
msgid "Testing Mode (testMode)"
msgstr ""

#: worldpay.module:57
msgid "WorldPay testMode code. Set to 0 for live shop, 100 or 101 for testing."
msgstr ""

#: worldpay.module:64
msgid "Currency code (currency)"
msgstr ""

#: worldpay.module:67
msgid "The currecy code that WorldPay should process the payment in."
msgstr ""

#: worldpay.module:73
msgid "WorldPay Minimum Amount"
msgstr ""

#: worldpay.module:77
msgid "The minimum purchase amount (gross) that will be accepted for WorldPay payments."
msgstr ""

#: worldpay.module:83
msgid "Worldpay checkout settings"
msgstr ""

#: worldpay.module:91
msgid "WorldPay processing URL"
msgstr ""

#: worldpay.module:95
msgid "URL of the secure payment page customers are sent to for payment processing (Select Junior). If unsure leave at default setting."
msgstr ""

#: worldpay.module:100
msgid "Thank you page/WorldPay callback URL"
msgstr ""

#: worldpay.module:104
msgid "<P><strong>BASIC USAGE:</strong> Redirect users to a specific thank you page and process your orders manually from Worldpay emails and your worldpay account.</p><P><strong>ADVANCED USAGE:</strong> Insert <strong>worldpay_callback</strong> as the link that is used for processing payment confirmation (Select Junior). This also needs to be configured in the settings on the WorldPay server as the full URL. Please refer to the README.TXT that came with the worldpay.module for more detailed instructions.</p>"
msgstr ""

#: worldpay.module:108
msgid "Worldpay_callback - Payment Completed message"
msgstr ""

#: worldpay.module:112
msgid "Insert your own message that is displayed when a payment is completed using worldpay_callback"
msgstr ""

#: worldpay.module:116
msgid "Worldpay_callback - Order Cancelled message"
msgstr ""

#: worldpay.module:120
msgid "Insert your own message that is displayed when a payment is cancelled using worldpay_callback"
msgstr ""

#: worldpay.module:124
msgid "Worldpay_callback - server validation check failed message"
msgstr ""

#: worldpay.module:128
msgid "Insert your own message that is displayed when the Worldpay server validation check fails using worldpay_callback - a possible security breach"
msgstr ""

#: worldpay.module:132
msgid "Switch debug on to display Worldpay feedback and data while testing"
msgstr ""

#: worldpay.module:135
msgid "If this option is enabled, Worldpay callback data will be displayed in full at the bottom of your results page."
msgstr ""

#: worldpay.module:150
msgid "Credit/Debit Card (WorldPay)"
msgstr ""

#: worldpay.module:179
msgid "WorldPay callback"
msgstr ""

#: worldpay.module:207
msgid "Your purchase total must be at least %min-purchase-amount for online Credit card purchases."
msgstr ""

#: worldpay.module:288
msgid "<h1>PAYMENT COMPLETED!</h1><p>WorldPay(tm) has accepted your card and payment has been made. Thank you. Click through to your %link to view/print out invoices and track your order.</p>"
msgstr ""

#: worldpay.module:303
msgid "<h1>PAYMENT CANCELLED!</h1><p>WorldPay(tm) has indicated that you have cancelled your order. Please click to %link and contact us or try again.</p>"
msgstr ""

#: worldpay.module:311
msgid "<h1>Sorry!</h1><p>We were unable to validate your request with the WorldPay(tm) server. Please click through to our %link to try again or to contact us if you are having difficulties.</p>"
msgstr ""

#: worldpay.module:0
msgid "worldpay"
msgstr ""

#: worldpay.info:0
msgid "WorldPay"
msgstr ""

#: worldpay.info:0
msgid "WorldPay payment integration for the ecommerce.module"
msgstr ""

#: worldpay.info:0
msgid "E-Commerce Payment Methods"
msgstr ""

