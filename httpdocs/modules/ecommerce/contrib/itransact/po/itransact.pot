# $Id: itransact.pot,v 1.1.2.1 2007/05/03 15:12:24 darrenoh Exp $
#
# LANGUAGE translation of Drupal (itransact.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  itransact.module,v 1.2.2.3.2.1.2.6 2007/02/16 01:40:57 sime
#  itransact.info,v 1.2.2.8 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 11:06-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: itransact.module:11
msgid "Enter the required parameters that have been supplied during the signup process with ccard."
msgstr ""

#: itransact.module:20
msgid "Enter Credit Card Details"
msgstr ""

#: itransact.module:28
msgid "ITransact payment gateway connection settings."
msgstr ""

#: itransact.module:42
msgid "iTransact client id"
msgstr ""

#: itransact.module:46
msgid "client id that was issued by iTransact"
msgstr ""

#: itransact.module:51
msgid "iTransact client password"
msgstr ""

#: itransact.module:55
msgid "client password that was issued by iTransact"
msgstr ""

#: itransact.module:60
msgid "Credit Card Payment Page"
msgstr ""

#: itransact.module:64
msgid "URL to be directed to so that the payment can be received."
msgstr ""

#: itransact.module:69
msgid "Thank you Page"
msgstr ""

#: itransact.module:73
msgid "URL to be directed once the payment has been entered."
msgstr ""

#: itransact.module:78
msgid "CVV field is required"
msgstr ""

#: itransact.module:81
msgid "CVV is not supported at this time. If you would like this feature added, please sponsor a Drupal developer to do it."
msgstr ""

#: itransact.module:85
msgid "Address field is required"
msgstr ""

#: itransact.module:88
msgid "AVS is not supported at this time. If you would like this feature added, please sponsor a Drupal developer to do it."
msgstr ""

#: itransact.module:97
msgid "Credit Card"
msgstr ""

#: itransact.module:151
msgid "submit payment"
msgstr ""

#: itransact.module:195
msgid "your payment has been accepted, thank you"
msgstr ""

#: itransact.module:317
msgid "Payment attempted; Gateway error. Try again in a few minutes and if it still fails contact the system administrator."
msgstr ""

#: itransact.module:321
msgid "Card declined"
msgstr ""

#: itransact.module:326
msgid "Invalid XML Format. Contact the System Administrator. No transactions will be processed until this is fixed. Error code: "
msgstr ""

#: itransact.module:330
msgid "Payment attempted; Invalid address supplied"
msgstr ""

#: itransact.module:334
msgid "Payment attempted; Invalid CVN provided"
msgstr ""

#: itransact.module:0
msgid "itransact"
msgstr ""

#: itransact.info:0
msgid "iTransact"
msgstr ""

#: itransact.info:0
msgid "iTransact payment gateway in E-Commerce."
msgstr ""

#: itransact.info:0
msgid "E-Commerce Payment Methods"
msgstr ""

