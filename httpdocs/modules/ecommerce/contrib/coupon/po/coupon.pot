# $Id: coupon.pot,v 1.1.2.1 2007/05/03 15:12:22 darrenoh Exp $
#
# LANGUAGE translation of Drupal (coupon.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  coupon.module,v 1.6.2.2.2.8.2.12 2007/03/03 13:32:22 gordon
#  coupon.info,v 1.3.2.10 2007/02/25 13:54:57 gordon
#  coupon.install,v 1.2.4.4.2.3 2007/02/15 06:04:32 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:57-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: coupon.module:13
msgid "Creates Gift Certificates for customers to use for later purchases."
msgstr ""

#: coupon.module:17
msgid "If a value of $0.00 is nominated then users will have a form which allow the entry of the value of the Gift Certificate. If an amount is nominated then this will be to value that is added."
msgstr ""

#: coupon.module:29
msgid "Coupons"
msgstr ""

#: coupon.module:31
msgid "Manage gift certificates and coupons"
msgstr ""

#: coupon.module:35
msgid "List"
msgstr ""

#: coupon.module:40
msgid "Create"
msgstr ""

#: coupon.module:48
msgid "Delete coupon"
msgstr ""

#: coupon.module:66
msgid "Configure coupons"
msgstr ""

#: coupon.module:76
msgid "Coupon e-mail"
msgstr ""

#: coupon.module:76
msgid "This text will be emailed to customer after they buy a coupon."
msgstr ""

#: coupon.module:86
msgid "Gift Certificate"
msgstr ""

#: coupon.module:103;172
msgid "Amount"
msgstr ""

#: coupon.module:168
msgid "Options"
msgstr ""

#: coupon.module:176
msgid "How much would you like the Gift Certificate to be?"
msgstr ""

#: coupon.module:185
msgid "add to cart"
msgstr ""

#: coupon.module:213
msgid "remove"
msgstr ""

#: coupon.module:219
msgid "Coupon/Gift Certificate Number"
msgstr ""

#: coupon.module:225
msgid "Add"
msgstr ""

#: coupon.module:231
msgid "Invalid or Previously Used Coupon/Gift Certificate"
msgstr ""

#: coupon.module:236
msgid "Coupon/Gift Certificate has already been added to this order"
msgstr ""

#: coupon.module:315
msgid "Coupon No."
msgstr ""

#: coupon.module:316
msgid "Discount"
msgstr ""

#: coupon.module:331
msgid "Redeem Coupon/Gift Certificates"
msgstr ""

#: coupon.module:410 coupon.info:0
msgid "Coupon"
msgstr ""

#: coupon.module:411
msgid "Type of Discount"
msgstr ""

#: coupon.module:412
msgid "Discount Paid"
msgstr ""

#: coupon.module:413
msgid "Discount Available"
msgstr ""

#: coupon.module:414
msgid "Created"
msgstr ""

#: coupon.module:415
msgid "Transaction"
msgstr ""

#: coupon.module:442;546
msgid "Delete"
msgstr ""

#: coupon.module:472
msgid "Value of Coupons"
msgstr ""

#: coupon.module:475
msgid "Enter the value of the coupons to be created. eg. 10%, a set value of 5.00"
msgstr ""

#: coupon.module:479
msgid "Number of Coupons to Create"
msgstr ""

#: coupon.module:485
msgid "Select output method"
msgstr ""

#: coupon.module:486
msgid "Show on Page"
msgstr ""

#: coupon.module:486
msgid "Printable Page"
msgstr ""

#: coupon.module:486
msgid "CSV File"
msgstr ""

#: coupon.module:490
msgid "Create Coupons"
msgstr ""

#: coupon.module:497
msgid "Coupon Value is a required field"
msgstr ""

#: coupon.module:500
msgid "Coupon Value is not in a valid format"
msgstr ""

#: coupon.module:504
msgid "Coupon Count is a required field and must be a positive numeric"
msgstr ""

#: coupon.module:546
msgid "Do you want to delete coupon %coupon"
msgstr ""

#: coupon.module:546
msgid "This coupon will not longer be able to be redeemed once it has been deleted."
msgstr ""

#: coupon.module:551
msgid "Coupon %coupon has been deleted"
msgstr ""

#: coupon.module:581
msgid "Gift certificate number."
msgstr ""

#: coupon.module:582
msgid "Gift certificate discount amount."
msgstr ""

#: coupon.module:608
msgid "Coupon or gift certificate delivery mail"
msgstr ""

#: coupon.module:618
msgid "Gift Certificate for %site"
msgstr ""

#: coupon.module:619
msgid "Dear %billing_name\n\nHere is your gift certificate.\nTo redeem this Certificate please the following number during your next purchase to get a discount of up to %discount_amount.\n\n%coupon_number"
msgstr ""

#: coupon.module:0
msgid "coupon"
msgstr ""

#: coupon.install:50
msgid "E-Commerce: Coupon tables have been created."
msgstr ""

#: coupon.info:0
msgid "Price adjustment module for E-Commerce."
msgstr ""

#: coupon.info:0
msgid "E-Commerce Uncategorized"
msgstr ""

