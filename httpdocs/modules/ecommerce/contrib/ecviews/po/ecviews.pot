# $Id: ecviews.pot,v 1.1.2.1 2007/05/03 15:12:24 darrenoh Exp $
#
# LANGUAGE translation of Drupal (ecviews.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  ecviews.module,v 1.5.4.2 2007/02/19 04:40:21 neclimdul
#  ecviews.info,v 1.3.4.8 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 11:04-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ecviews.module:16
msgid "Product: List of products"
msgstr ""

#: ecviews.module:40;56
msgid "Product: SKU"
msgstr ""

#: ecviews.module:44;59
msgid "Product: Price"
msgstr ""

#: ecviews.module:48
msgid "Product: Add to cart link"
msgstr ""

#: ecviews.module:83
msgid "Filter the product based upon the product being a normal product or a sub-product"
msgstr ""

#: ecviews.module:107;128
msgid "Purchased Product: Title"
msgstr ""

#: ecviews.module:111;131
msgid "Purchased Product: Quantity"
msgstr ""

#: ecviews.module:115;134
msgid "Purchased Product: Price"
msgstr ""

#: ecviews.module:119;137
msgid "Purchased Product: Number of Purchases"
msgstr ""

#: ecviews.info:0
msgid "EC Views"
msgstr ""

#: ecviews.info:0
msgid "Views for E-Commerce."
msgstr ""

#: ecviews.info:0
msgid "E-Commerce Uncategorized"
msgstr ""

