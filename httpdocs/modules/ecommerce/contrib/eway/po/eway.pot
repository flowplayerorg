# $Id: eway.pot,v 1.1.2.1 2007/05/03 15:12:24 darrenoh Exp $
#
# LANGUAGE translation of Drupal (eway.inc)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  eway.inc,v 1.2.2.1 2006/09/20 11:39:43 sammys
#  eway.module,v 1.4.2.5.2.1.2.5 2007/02/10 01:01:00 gordon
#  eway.info,v 1.2.2.8 2007/02/25 14:03:35 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 11:05-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: eway.inc:106
msgid "xml_payment"
msgstr ""

#: eway.module:11
msgid "Enter the required parameters that have been supplied during the signup process with eway."
msgstr ""

#: eway.module:20
msgid "Enter Credit Card Details"
msgstr ""

#: eway.module:42
msgid "E-Way Client Id"
msgstr ""

#: eway.module:46
msgid "Client Id that was issued by E Way"
msgstr ""

#: eway.module:51
msgid "Credit Card Payment Page"
msgstr ""

#: eway.module:55
msgid "URL to be directed to so that the payment can be received."
msgstr ""

#: eway.module:60
msgid "Thank you Page"
msgstr ""

#: eway.module:64
msgid "URL to be directed once the payment has been entered."
msgstr ""

#: eway.module:73
msgid "Credit Card"
msgstr ""

#: eway.module:125
msgid "submit payment"
msgstr ""

#: eway.module:164
msgid "your payment has been accepted, thank you"
msgstr ""

#: eway.module:261
msgid "Payment attempted; Invalid card number"
msgstr ""

#: eway.module:265
msgid "Payment attempted; Invalid expiry date"
msgstr ""

#: eway.module:269
msgid "Invalid Client ID. Contact the System Administrator. No transactions will be processed until this is fixed."
msgstr ""

#: eway.module:273
msgid "Payment attempted; Invalid amount supplied"
msgstr ""

#: eway.module:277
msgid "Payment attempted; Invalid CVN provided"
msgstr ""

#: eway.module:0
msgid "eway"
msgstr ""

#: eway.info:0
msgid "Eway"
msgstr ""

#: eway.info:0
msgid "Eway payment gateway in E-Commerce."
msgstr ""

#: eway.info:0
msgid "E-Commerce Payment Methods"
msgstr ""

