# $Id: cod.pot,v 1.1.2.1 2007/05/03 15:12:22 darrenoh Exp $
#
# LANGUAGE translation of Drupal (cod.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  cod.module,v 1.8.2.2.2.2.2.7 2007/03/07 05:38:18 neclimdul
#  cod.info,v 1.3.2.8 2007/02/25 13:54:57 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:56-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: cod.module:16
msgid "C.O.D. stands for \"Cash on Delivery\". Set the options for\n                        this simple payment method, including renaming it."
msgstr ""

#: cod.module:33
msgid "Title to use COD"
msgstr ""

#: cod.module:34;75
msgid "COD"
msgstr ""

#: cod.module:38
msgid "Description to show shoppers for this type of payment."
msgstr ""

#: cod.module:42
msgid "Page to go to after payment"
msgstr ""

#: cod.module:47
msgid "Page to go to after payment is completed."
msgstr ""

#: cod.module:51
msgid "Mark transaction as paid?"
msgstr ""

#: cod.module:53;60
msgid "No"
msgstr ""

#: cod.module:53;60
msgid "Yes"
msgstr ""

#: cod.module:54
msgid "Whether to mark transactions as paid immediately, or wait till the store admin marks them as paid"
msgstr ""

#: cod.module:58
msgid "Mark transaction workflow as completed?"
msgstr ""

#: cod.module:61
msgid "Marks transactions that have no shippable items as completed in the workflow"
msgstr ""

#: cod.module:90
msgid "Your purchase has been completed."
msgstr ""

#: cod.module:116
msgid "Your purchase has been received."
msgstr ""

#: cod.module:127
msgid "Make a COD Payment"
msgstr ""

#: cod.info:0
msgid "C.O.D."
msgstr ""

#: cod.info:0
msgid "Generic manual payment method for E-Commerce."
msgstr ""

#: cod.info:0
msgid "E-Commerce Payment Methods"
msgstr ""

